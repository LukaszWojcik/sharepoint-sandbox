﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Csom
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        private ClientContext _context = null;

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Shown(object sender, EventArgs e)
        {
            button1.PerformClick();
            comboBox1.SelectedItem = comboBox1.Items.Cast<List>().FirstOrDefault(l => l.Title == "My documents");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _context = new ClientContext(textBox1.Text);

            comboBox1.DataSource = GetRootFolders(_context, _context.Web.Lists).ToList();
            comboBox1.DisplayMember = nameof(List.Title);

            //var list = context.Web.Lists.GetByTitle("My documents");

            //context.Load(list);
            //context.Load(list.RootFolder);
            ////context.Load(list.RootFolder.Folders);
            //context.Load(list.Views);
            //context.Load(list.RootFolder.Files);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var list = (sender as ComboBox)?.SelectedItem as List;
            if (list != null)
            {
                _context.Load(list);
                _context.Load(list.Views);
                _context.ExecuteQuery();
                comboBox2.DataSource = list.Views.ToList();
                comboBox2.DisplayMember = nameof(List.Title);
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            imageList1.Images.OfType<Image>().ToList().ForEach(img => img.Dispose());
            imageList1.Images.Clear();

            var view = (sender as ComboBox)?.SelectedItem as Microsoft.SharePoint.Client.View;
            if (view != null)
            {
                var list = comboBox1.SelectedItem as List;

                var caml = new CamlQuery();
                caml.ViewXml = view.ViewQuery;
                var items = list.GetItems(caml);

                //System.Diagnostics.Debugger.Break();
                //return;

                //_context.Load(list);
                _context.Load(items, lists => lists.Include(item => item.DisplayName));
                //_context.Load(list.RootFolder);
                //_context.Load(list.RootFolder.Files);
                _context.ExecuteQuery();
                //try
                //{
                foreach (var item in items)
                {
                    var icoId = Guid.NewGuid().ToString();
                    try
                    {
                        var icoName = _context.Web.MapToIcon(item.DisplayName, "", Microsoft.SharePoint.Client.Utilities.IconSize.Size16);
                        _context.ExecuteQuery();
                        using (var webClient = new System.Net.WebClient())
                        {
                            var data = webClient.DownloadData($"{textBox1.Text}/_layouts/images/{icoName.Value}");
                            using (var mem = new System.IO.MemoryStream(data))
                            {
                                imageList1.Images.Add(icoId, Image.FromStream(mem));
                            }
                        }
                    }
                    catch { }

                    var newItem = listView1.Items.Add(item.DisplayName, icoId);
                    newItem.Checked = true;
                }
                //}
                //catch (Exception ex)
                //{
                //    //MessageBox.Show(ex.Message);
                //}
            }
        }

        private ListCollection GetRootFolders(ClientContext context, ListCollection collection)
        {
            context.Load(collection, lists =>
                lists.Include(
                    list => list.Title,
                    list => list.Created,
                    list => list.Fields.Include(
                        field => field.InternalName,
                        field => field.TypeAsString)).Where(
                    list => list.BaseType == BaseType.DocumentLibrary));
            context.ExecuteQuery();
            return collection;
        }

        private FileCollection GetFiles(ClientContext context, ListCollection collection)
        {
            return null;
        }


        //private FolderCollection GetSubfolders()
        //{
        //}
    }
}
