﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Csom
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            return;


            string siteUrl = "http://vmsp2013:17705/";

            var clientContext = new ClientContext(siteUrl);

            var web = clientContext.Web;
            var collection = web.Lists;

            clientContext.Load(collection, lists =>
                lists.Include(
                    list => list.Title,
                    list => list.Created,
                    list => list.Fields.Include(
                        field => field.InternalName,
                        field => field.TypeAsString)).Where(
                    list => list.BaseType == BaseType.DocumentLibrary 
                    && list.Title == "My documents"));
            clientContext.ExecuteQuery();

            var fileList = collection[0];

            foreach (var item in fileList.GetItems(new CamlQuery()))
            {
                Console.WriteLine(item.DisplayName);
                //var fields = list.Fields;
                //foreach (var field in list.Fields)
                //{
                    //Console.WriteLine("Title: {0} Created: {1}", list.Title, list.Created.ToString());
                //    Console.WriteLine("[ {1} ]\t{0}", field.InternalName, field.TypeAsString.PadRight(list.Fields.ToList().Max(f => f.TypeAsString.Length)));
                //}
            }

            Console.Read();
        }
    }
}
