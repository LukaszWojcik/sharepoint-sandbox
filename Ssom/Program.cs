﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ssom
{
    class Program
    {
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ShowWindow(System.IntPtr hWnd, int cmdShow);

        static void Main(string[] args)
        {
            Console.WindowWidth = 256;
            ShowWindow(System.Diagnostics.Process.GetCurrentProcess().MainWindowHandle, 3);

            using (var site = new SPSite("http://vmsp2013:17705"))
            {
                using (var web = site.OpenWeb())
                {
                    if (web != null)
                    {
                        var list = web.Lists["My list"];
                        var items = list.GetItems();
                        items.OfType<SPListItem>().ToList().ForEach(item => Console.WriteLine("{0}\r\n", string.Join("\t", item.Fields.OfType<SPField>().Where(field => field.Hidden == false).Select(field => item[field.Id]))));
                    }
                }
            }
            Console.Read();
        }
    }
}
