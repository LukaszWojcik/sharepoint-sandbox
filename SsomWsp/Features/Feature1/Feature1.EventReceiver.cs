using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;

namespace SsomWsp.Features.Feature1
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("7ba361e7-ce6b-42bb-95ed-bb2c44a48829")]
    public class Feature1EventReceiver : SPFeatureReceiver
    {
        public const string DiscoListTitle = "Disco";
        public const string RelaxListTitle = "Relax";

        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            using (var web = (SPWeb)properties.Feature.Parent)
            {
                var discoListGuid = web.Lists.Add(DiscoListTitle, "", SPListTemplateType.GenericList);
                var relaxListGuid = web.Lists.Add(RelaxListTitle, "", SPListTemplateType.GenericList);

                var discoList = web.Lists[discoListGuid];
                discoList.EventReceivers.Add(SPEventReceiverType.ItemAdded, this.GetType().Assembly.FullName, typeof(SsomWsp.EventReceivers.DiscoListEventReceiver.DiscoListEventReceiver).FullName);

                SendEmail(web, properties.Feature, "Feature activated.");
            }
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            using (var web = (SPWeb)properties.Feature.Parent)
            {
                if (web.Lists.TryGetList(DiscoListTitle) != null) web.Lists.TryGetList(DiscoListTitle).Delete(); //web.Lists.TryGetList(DiscoListTitle)?.Delete();
                if (web.Lists.TryGetList(RelaxListTitle) != null) web.Lists.TryGetList(RelaxListTitle).Delete(); //web.Lists.TryGetList(RelaxListTitle)?.Delete();

                SendEmail(web, properties.Feature, "Feature deactivated.");
            }
        }


        private static void SendEmail(SPWeb web, SPFeature feature, string message)
        {
            var result = SPUtility.SendEmail(web, true, true, "lukasz.wojcik@findwise.com", string.Format("{0} notification", feature.Definition.GetTitle(System.Globalization.CultureInfo.CurrentCulture)), message);
            
        }


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
