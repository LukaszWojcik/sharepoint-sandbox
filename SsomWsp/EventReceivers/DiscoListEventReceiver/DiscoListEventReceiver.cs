﻿using System;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;

namespace SsomWsp.EventReceivers.DiscoListEventReceiver
{
    /// <summary>
    /// List Item Events
    /// </summary>
    public class DiscoListEventReceiver : SPItemEventReceiver
    {
        /// <summary>
        /// An item was added.
        /// </summary>
        public override void ItemAdded(SPItemEventProperties properties)
        {
            base.ItemAdded(properties);

            using (var web = properties.Web)
            {
                if (web.Lists.TryGetList(SsomWsp.Features.Feature1.Feature1EventReceiver.RelaxListTitle) != null) //web.Lists.TryGetList(SsomWsp.Features.Feature1.Feature1EventReceiver.RelaxListTitle)?.Delete();
                {
                    var item = web.Lists.TryGetList(SsomWsp.Features.Feature1.Feature1EventReceiver.RelaxListTitle).AddItem();
                    item["Title"] = properties.ListItem.Title;
                    item.Update();
                }
            }
        }


    }
}